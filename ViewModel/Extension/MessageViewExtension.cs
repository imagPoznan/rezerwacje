﻿using MaterialDesignThemes.Wpf;
using Rezerwacje.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rezerwacje.ViewModel.Extension
{
    class MessageViewExtension
    {
        public static async Task ShowMessage(string message, string dialogHost = "MainDialog")
        {
            var msgBox = new MessageView()
            {
                Message = { Text = message },
                Header = { Header = "Komunikat" },
                Icon = { Kind = PackIconKind.Message }
            };
            _ = await DialogHost.Show(msgBox, dialogHost);
        }
        public static async Task ShowError(string message, string dialogHost = "MainDialog")
        {
            var msgBox = new MessageView()
            {
                Message = { Text = message },
                Header = { Header = "Błąd" },
                Icon = { Kind = PackIconKind.Error }
            };
            _ = await DialogHost.Show(msgBox, dialogHost);
        }
        public static async Task ShowWarning(string message, string dialogHost = "MainDialog")
        {
            var msgBox = new MessageView()
            {
                Message = { Text = message },
                Header = { Header = "Ostrzeżenie" },
                Icon = { Kind = PackIconKind.Warning }
            };
            _ = await DialogHost.Show(msgBox, dialogHost);
        }
    }
}
