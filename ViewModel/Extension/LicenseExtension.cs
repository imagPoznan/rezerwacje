﻿using Rezerwacje.WebReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Rezerwacje.ViewModel.Extension
{
    public class LicenseExtension
    {
        public enum LicencjaStatus
        {
            Potwierdzona,
            Brak,
            Tymczasowa
        }
        public static bool GetLicense(string numerLicencji, out LicencjaStatus licencjaType, out DateTime? dataWygasniecia)
        {
            try
            {
                service licencja = new service { Url = "http://195.160.180.44/LicenseManager.svc?wsdl" };
                string waproMag = WaproExtension.Instance.GetWaproDatabaseId();
                LicenseObject response = licencja.GetLicense(numerLicencji, waproMag, "ZamowieniaPrzyszle");
                licencjaType = response != null && response.Response ? LicencjaStatus.Potwierdzona : LicencjaStatus.Brak;
                dataWygasniecia = response != null && response.DataWygasnieciaSpecified
                    ? response.DataWygasniecia
                    : null;
                return response != null && response.Response;
                throw new Exception();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static bool ActiveLicense(string numerLicencji, out LicencjaStatus licencjaType, out string licencjaResponseDesc, out DateTime? licencjaResponseDataWygasniecia)
        {
            try
            {
                service licencja = new service { Url = "http://195.160.180.44/LicenseManager.svc?wsdl" };

                string waproMag = WaproExtension.Instance.GetWaproDatabaseId();

                if (string.IsNullOrEmpty(numerLicencji) || string.IsNullOrEmpty(waproMag))
                {
                    throw new ArgumentException();
                }

                LicenseObject response = licencja.ActiveLicense(numerLicencji, waproMag, "ZamowieniaPrzyszle");

                licencjaType = response != null && response.Response ? LicencjaStatus.Potwierdzona : LicencjaStatus.Brak;

                licencjaResponseDesc = response?.Description;
                licencjaResponseDataWygasniecia = response != null && response.DataWygasnieciaSpecified ? response.DataWygasniecia : null;

                return response != null && response.Response;
            }
            catch (ArgumentException)
            {
                licencjaType = LicencjaStatus.Brak;
                licencjaResponseDesc = "Brak numeru licencji lub brak połączenia z bazą danych WF-MAG";
                licencjaResponseDataWygasniecia = null;
                return false;
            }
            catch (Exception)
            {
                licencjaType = LicencjaStatus.Tymczasowa;
                licencjaResponseDesc = "Brak połączenia z API";
                licencjaResponseDataWygasniecia = null;
                return true;
            }
        }
    }
}
