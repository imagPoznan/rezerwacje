﻿using MaterialDesignThemes.Wpf;
using Rezerwacje.Model;
using Rezerwacje.View;
using Rezerwacje.ViewModel.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Rezerwacje.ViewModel
{
    class KolejkaRezerwacjiViewModel : ViewModelExtension
    {
        private PozycjaDokumentuModel _dostawa;
        private LicenseModel _licencja;
        public decimal IdPozDostawy { get; set; }
        public decimal Ilosc { get; set; }
        public decimal IloscDostepna { get; set; }
        public decimal Zarezerwowano { get; set; }
        public decimal Zrealizowano { get; set; }
        public PozycjaDokumentuModel Dostawa { get => _dostawa; set { _dostawa = value; OnPropertyChanged(); } }
        public LicenseModel Licencja { get => _licencja; set { _licencja = value; OnPropertyChanged(); } }
        public KolejkaRezerwacjiViewModel(decimal idPozDokMag)
        {
            IdPozDostawy = idPozDokMag;
            PobierzInformacjeDostawa(idPozDokMag);
            PobierzKolejkeRezerwacji(idPozDokMag);
        }
        private void PobierzInformacjeDostawa(decimal idPozDostawy)
        {
            try
            {
                Dostawa = new PozycjaDokumentuModel();
                using (SqlConnection sqlCnn = new SqlConnection(WaproExtension.Instance.GetWaproConnectionString()))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_PobierzPozycjeDostawy(" + idPozDostawy.ToString() + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            Dostawa.Przelicznik = Convert.ToDecimal(sqlRdr["Przelicznik"].ToString());
                            Dostawa.Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString());
                            Dostawa.ZarezerwowanoSztuk = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString());
                            Dostawa.PozostaloDoRezerwacji = Convert.ToDecimal(sqlRdr["PozostaloDoRezerwacji"].ToString());
                            Dostawa.PozostaloDoRezerwacjiSztuk = Convert.ToDecimal(sqlRdr["PozostaloDoRezerwacji"].ToString());
                            Dostawa.Ilosc = Convert.ToDecimal(sqlRdr["Ilosc"].ToString());
                            Dostawa.IloscSztuk = Convert.ToDecimal(sqlRdr["Ilosc"].ToString());
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private List<KolejkaRezerwacjiModel> _kolejkaRezerwacji;
        public List<KolejkaRezerwacjiModel> KolejkaRezerwacji { get => _kolejkaRezerwacji; set { _kolejkaRezerwacji = value; OnPropertyChanged(); } }
        private KolejkaRezerwacjiModel _zaznaczoneDoRezerwacji;
        public KolejkaRezerwacjiModel ZaznaczoneDoRezerwacji { get => _zaznaczoneDoRezerwacji; set { _zaznaczoneDoRezerwacji = value; OnPropertyChanged(); } }
        private void PobierzKolejkeRezerwacji(decimal idPozDokMag)
        {
            try
            {
                KolejkaRezerwacji = new List<KolejkaRezerwacjiModel>();
                using (SqlConnection sqlCnn = new SqlConnection(WaproExtension.Instance.GetWaproConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_PobierzKolejkeRezerwacji(" + idPozDokMag.ToString() + ")", sqlCnn);
                    using (SqlDataReader read = sqlCmd.ExecuteReader())
                    {
                        while (read.Read())
                        {
                            KolejkaRezerwacjiModel k = new KolejkaRezerwacjiModel
                            {
                                IdZamowienia = Convert.ToDecimal(read["IdZamowienia"].ToString()),
                                IdPozZamowienia = Convert.ToDecimal(read["IdPozZamowienia"].ToString()),
                                Numer = read["ZamowienieNumer"].ToString(),
                                NrZamKlienta = read["ZamowienieNrKlienta"].ToString(),
                                StanRealizacji = read["StanRealizacji"].ToString(),
                                Data = Convert.ToDateTime(read["ZamowienieData"].ToString()),
                                DataRealizacji = Convert.ToDateTime(read["ZamowienieDataRealizacji"].ToString()),
                                IdKontrahenta = Convert.ToDecimal(read["IdKontrahenta"].ToString()),
                                Kontrahent = read["KontrahentNazwa"].ToString(),
                                Przelicznik = Convert.ToDecimal(read["Przelicznik"].ToString()),
                                Zamowiono = Convert.ToDecimal(read["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(read["Zrealizowano"].ToString()),
                                Jednostka = read["Jednostka"].ToString(),
                                ZamowionoSztuk = Convert.ToDecimal(read["ZamowionoSzt"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(read["Zarezerwowano"].ToString()),
                                ZarezerwowanoSztuk = Convert.ToDecimal(read["ZarezerwowanoSztuk"].ToString()),
                                Lp = Convert.ToInt32(read["Lp"].ToString())
                            };

                            KolejkaRezerwacji.Add(k);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem kolejki rezerwacji. Błąd: " + ex.Message, "SecondaryDialog");
                throw;
            }
        }
        public ICommand ZarezerwujPozycjeCommand => new RelayCommandExtension(ZarezerwujPozycje);
        private void ZarezerwujPozycje(object o)
        {
            int result = 0;
            try
            {
                if (ZaznaczoneDoRezerwacji == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono pozycji do rezerwacji!", "SecondaryDialog");
                }
                else
                {
                    if (Dostawa.Zarezerwowano+ZaznaczoneDoRezerwacji.DoRezerwacjiSztuk>Dostawa.IloscSztuk)
                    {
                        _ = MessageViewExtension.ShowWarning("Ilość do rezerwacji nie może być większa niż ilość dostawy!", "SecondaryDialog");
                    }
                    else
                    {
                        if (ZaznaczoneDoRezerwacji.StanRealizacji == "Z")
                        {
                            _ = MessageViewExtension.ShowWarning("Zamówienie " + ZaznaczoneDoRezerwacji.Numer + " zostało już całkowicie zrealizowane i nie można zmienić rezerwacji.", "SecondaryDialog");
                        }
                        if (ZaznaczoneDoRezerwacji.DoRezerwacji == ZaznaczoneDoRezerwacji.Zarezerwowano)
                        {
                            _ = MessageViewExtension.ShowWarning("Ilość do rezerwacji musi być różna od ilości zarezerwowanej!", "SecondaryDialog");
                        }
                        if (ZaznaczoneDoRezerwacji.DoRezerwacji > ZaznaczoneDoRezerwacji.Zamowiono-ZaznaczoneDoRezerwacji.Zrealizowano)
                        {
                            _ = MessageViewExtension.ShowWarning("Ilość do rezerwacji musi być mniejsza od ilości zamówiono-zrealizowano!", "SecondaryDialog");
                        }
                        if (ZaznaczoneDoRezerwacji.Zamowiono - ZaznaczoneDoRezerwacji.Zrealizowano - ZaznaczoneDoRezerwacji.Zarezerwowano <= 0)
                        {
                            _ = MessageViewExtension.ShowWarning("Ilość do zamówiono-zrealizowano-zarezerwowano musi być większa od 0!", "SecondaryDialog");
                        }
                        else
                        {
                            using (SqlConnection sqlCnn = new SqlConnection(WaproExtension.Instance.GetWaproConnectionString()))
                            {
                                SqlCommand sqlCmd = new SqlCommand("imag.kl_ZarezerwujPozycje", sqlCnn)
                                {
                                    CommandType = CommandType.StoredProcedure
                                };
                                SqlParameter pIdPozDostawy = sqlCmd.Parameters.AddWithValue("@IdPozDostawy", IdPozDostawy);
                                pIdPozDostawy.Direction = ParameterDirection.Input;
                                SqlParameter pIdPozZamowienia = sqlCmd.Parameters.AddWithValue("@IdPozZamowienia", ZaznaczoneDoRezerwacji.IdPozZamowienia);
                                pIdPozZamowienia.Direction = ParameterDirection.Input;
                                SqlParameter pDoRezerwacji = sqlCmd.Parameters.AddWithValue("@DoRezerwacji", ZaznaczoneDoRezerwacji.DoRezerwacjiSztuk);
                                pDoRezerwacji.Direction = ParameterDirection.Input;
                                SqlParameter pReturn = sqlCmd.Parameters.Add("ReturnVal", SqlDbType.Int);
                                pReturn.Direction = ParameterDirection.ReturnValue;

                                sqlCnn.Open();
                                //result = Convert.ToInt32(sqlCmd.ExecuteScalar());

                                sqlCmd.ExecuteNonQuery();
                                result = Convert.ToInt32(pReturn.Value.ToString());
                            }
                        }
                    }
                }
                if (result == 1)
                {
                    _ = MessageViewExtension.ShowMessage("Zmieniono rezerwację na pozycji zamówienia " + ZaznaczoneDoRezerwacji.Numer + ".", "SecondaryDialog");
                    PobierzInformacjeDostawa(IdPozDostawy);
                    PobierzKolejkeRezerwacji(IdPozDostawy);
                }
            }
            catch (Exception ex)
            {
                _ = MessageViewExtension.ShowError("Wystąpił błąd przy rezerwowaniu pozycji. " + ex.Message, "SecondaryDialog");
                throw;
            }
        }
        private RelayCommandExtension pokazKomunikatCommand;
        public ICommand PokazKomunikatCommand
        {
            get
            {
                if (pokazKomunikatCommand == null)
                {
                    pokazKomunikatCommand = new RelayCommandExtension(PokazKomunikat);
                }

                return pokazKomunikatCommand;
            }
        }
        private void PokazKomunikat(object commandParameter)
        {
            _ = MessageViewExtension.ShowError("Wystąpił błąd przy rezerwowaniu pozycji. ", "SecondaryDialog");
        }
    }
}
