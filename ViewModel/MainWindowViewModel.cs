﻿using MaterialDesignThemes.Wpf;
using Rezerwacje.Model;
using Rezerwacje.View;
using Rezerwacje.ViewModel.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZamowieniaPrzyszle.View;

namespace Rezerwacje.ViewModel
{
    class MainWindowViewModel : ViewModelExtension
    {
        public MainWindowViewModel()
        {
            PobierzPozycjeDokumentu(WaproExtension.Instance.IdObiektu);
        }
        private List<PozycjaDokumentuModel> _pozycjeDokumentu;
        private LicenseModel _license;
        public LicenseModel License { get => _license; set { _license = value; OnPropertyChanged(); } }
        public List<PozycjaDokumentuModel> PozycjeDokumentu { get => _pozycjeDokumentu; set { _pozycjeDokumentu = value; OnPropertyChanged(); } }
        private PozycjaDokumentuModel _zaznaczonaPozycja;
        public PozycjaDokumentuModel ZaznaczonaPozycja { get => _zaznaczonaPozycja; set { _zaznaczonaPozycja = value; OnPropertyChanged(); } }
        public void PobierzPozycjeDokumentu(decimal idObiektu)
        {
            try
            {
                PozycjeDokumentu = new List<PozycjaDokumentuModel>();
                using (SqlConnection sqlCnn = new SqlConnection(WaproExtension.Instance.GetWaproConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_PobierzPozycjeDokMag(" + idObiektu.ToString() + ")", sqlCnn);
                    using (SqlDataReader read = sqlCmd.ExecuteReader())
                    {
                        if (read.HasRows)
                        {
                            while (read.Read())
                            {
                                PozycjaDokumentuModel p = new PozycjaDokumentuModel
                                {
                                    Lp = Convert.ToInt32(read["Lp"].ToString()),
                                    Nazwa = read["ArtykulNazwa"].ToString(),
                                    IndeksHandlowy = read["ArtykulIndeksHandlowy"].ToString(),
                                    NazwaKoloru = read["ArtykulNazwaKoloru"].ToString(),
                                    NumerKoloru = read["ArtykulNumerKoloru"].ToString(),
                                    Przelicznik = Convert.ToDecimal(read["Przelicznik"].ToString()),
                                    Ilosc = Convert.ToDecimal(read["Ilosc"].ToString()),
                                    Jednostka = read["Jednostka"].ToString(),
                                    IloscSztuk = Convert.ToDecimal(read["IloscSztuk"].ToString()),
                                    CenaNetto = Convert.ToDecimal(read["CenaNetto"].ToString()),
                                    CenaBrutto = Convert.ToDecimal(read["CenaBrutto"].ToString()),
                                    IdPozDokMag = Convert.ToDecimal(read["IdPozDokMag"].ToString()),
                                    Zarezerwowano = Convert.ToDecimal(read["Zarezerwowano"].ToString()),
                                    PozostaloDoRezerwacji = Convert.ToDecimal(read["PozostaloDoRezerwacji"].ToString())
                                };
                                PozycjeDokumentu.Add(p);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Błąd przy pobieraniu pozycji dokumentu magazynowego. " + e.Message, "MainDialog");
                throw;
            }
        }
        public ICommand OtworzKolejkeRezerwacjiCommand => new RelayCommandExtension(OtworzKolejkeRezerwacji);
        private async void OtworzKolejkeRezerwacji(object o)
        {
            if (ZaznaczonaPozycja == null)
            {
                return;
            }
            else
            {
                KolejkaRezerwacjiView view = new KolejkaRezerwacjiView(ZaznaczonaPozycja.IdPozDokMag)
                {
                    DataContext = new KolejkaRezerwacjiViewModel(ZaznaczonaPozycja.IdPozDokMag)
                };
                object result = await DialogHost.Show(view, "MainDialog", ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
            }
        }
        private void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter is bool parameter &&
                parameter == false) return;

            //OK, lets cancel the close...
            eventArgs.Cancel();

            //...now, lets update the "session" with some new content!
            eventArgs.Session.UpdateContent(new SampleProgressDialog());
            //note, you can also grab the session when the dialog opens via the DialogOpenedEventHandler

            //lets run a fake operation for 3 seconds then close this baby.
            Task.Delay(TimeSpan.FromSeconds(0))
                .ContinueWith((t, _) => eventArgs.Session.Close(false), null,
                    TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void ExtendedOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs) { }

        private RelayCommandExtension licencjaCommand;

        public ICommand LicencjaCommand
        {
            get
            {
                if (licencjaCommand == null)
                {
                    licencjaCommand = new RelayCommandExtension(Licencja);
                }

                return licencjaCommand;
            }
        }

        private async void Licencja(object commandParameter)
        {
            LicenseView view = new LicenseView();
            object result = await DialogHost.Show(view, "MainDialog", ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
        }

        private RelayCommandExtension aktywujLicencjeCommand;
        public ICommand AktywujLicencjeCommand
        {
            get
            {
                if (aktywujLicencjeCommand == null)
                {
                    aktywujLicencjeCommand = new RelayCommandExtension(AktywujLicencje);
                }

                return aktywujLicencjeCommand;
            }
        }

        private void AktywujLicencje(object commandParameter)
        {
            License.IsActive = LicenseExtension.ActiveLicense(License.NumerLicencji, out LicenseExtension.LicencjaStatus licencjaStatus, out string licResponse, out DateTime? dataWygasniecia);
            License.Status = licencjaStatus;
            License.DataWygasniecia = dataWygasniecia?.ToLongDateString() ?? "-";
        }
    }
}
