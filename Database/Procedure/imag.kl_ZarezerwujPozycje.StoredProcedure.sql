/****** Object:  StoredProcedure [imag].[kl_ZarezerwujPozycje]    Script Date: 19.07.2021 08:40:04 ******/
DROP PROCEDURE IF EXISTS [imag].[kl_ZarezerwujPozycje]
GO
/****** Object:  StoredProcedure [imag].[kl_ZarezerwujPozycje]    Script Date: 19.07.2021 08:40:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol Łyduch
-- Create date: 2021-07-15
-- Description:	Procedura rezerwuje zamówienia
-- w wybranej pozycji dostawy
-- =============================================
CREATE	PROCEDURE [imag].[kl_ZarezerwujPozycje]
		-- Add the parameters for the stored procedure here
		@IdPozDostawy numeric
		,@IdPozZamowienia numeric
		,@DoRezerwacji decimal(16,6)
AS
begin
	set	xact_abort, nocount on
	begin try
		--deklaracja transakcji
		begin	transaction kl_ZarezerwujPozycje
		--deklaracja zmiennych
		declare	@Message nvarchar(2048)=''
				,@IdFirmy numeric
				,@IdMagazynu numeric
				,@IdUzytkownika numeric=3000001

				,@IdZamowienia numeric
				,@IdArtykulu numeric
				,@KodVAT char(3)
				,@Zamowiono decimal(16,6)
				,@Zrealizowano decimal(16,6)
				,@Zarezerwowano decimal(16,6)
				,@CenaNetto decimal(14,4)
				,@CenaBrutto decimal(14,4)
				,@CenaNettoWal decimal(14,4)
				,@CenaBruttoWal decimal(14,4)
				,@Przelicznik decimal(16,6)
				,@Jednostka varchar(10)
				,@Narzut decimal(8,4)
				,@Opis varchar(500)
				,@ZnakNarzutu tinyint
				,@IdWariantu numeric
				,@ZnacznikCeny char(1)
				,@NrSerii varchar(50)

				,@IdKontrahenta numeric
				,@IdTypu numeric
				,@Numer varchar(30)
				,@Dokument tinyint=2
				,@NumeracjaFormat varchar(30)
				,@NumeracjaOkres tinyint
				,@NumeracjaAuto tinyint
				,@NumeracjaNiezalezny tinyint
				,@Autonumer int
				,@Data int
				,@DataRealizacji int
				,@Zaliczka decimal(14,4)
				,@Priorytet tinyint
				,@AutoRezerwacja tinyint
				,@NrZamKlienta varchar(30)
				,@Typ tinyint
				,@IdPracownika numeric
				,@PrzelicznikWal decimal(14,6)
				,@DataKursWal int
				,@SymWal char(3)
				,@DokWal tinyint
				,@FlagaStanu tinyint
				,@TrybRejestracji tinyint
				,@RabatNarzut decimal(8,4)
				,@ZnakRabatu tinyint
				,@Uwagi varchar(1000)
				,@IdRachunku numeric
				,@InformacjeDodatkowe varchar(1000)
				,@OsobaZamawiajaca varchar(101)
				,@IdKontaktu numeric
				,@FormaPlatnosci varchar(50)
				,@DniPlatnosci int
				,@FakturaParagon tinyint
				,@NumerPrzesylki varchar(50)
				,@IdOperatoraPrzesylki numeric
											
		select	@IdZamowienia=ID_ZAMOWIENIA
				,@IdArtykulu=ID_ARTYKULU
				,@KodVAT=KOD_VAT
				,@Zamowiono=ZAMOWIONO
				,@Zrealizowano=ZREALIZOWANO
				,@Zarezerwowano=ZAREZERWOWANO
				,@CenaNetto=CENA_NETTO
				,@CenaBrutto=CENA_BRUTTO
				,@CenaNettoWal=CENA_NETTO_WAL
				,@CenaBruttoWal=CENA_BRUTTO_WAL
				,@Przelicznik=PRZELICZNIK
				,@Jednostka=JEDNOSTKA
				,@Narzut=NARZUT
				,@Opis=OPIS
				,@ZnakNarzutu=case when NARZUT>=0 then 2 else 0 end
				,@IdWariantu=ID_WARIANTU
				,@ZnacznikCeny=ZNACZNIK_CENY
				,@NrSerii=NR_SERII

		from	dbo.POZYCJA_ZAMOWIENIA
		where	ID_POZYCJI_ZAMOWIENIA=@IdPozZamowienia

		select	@IdFirmy=ID_FIRMY
				,@IdMagazynu=ID_MAGAZYNU
				,@IdKontrahenta=ID_KONTRAHENTA
				,@Numer=NUMER
				,@Autonumer=AUTONUMER
				,@Data=DATA
				,@DataRealizacji=DATA_REALIZACJi
				,@Zaliczka=ZALICZKA
				,@Priorytet=PRIORYTET
				,@AutoRezerwacja=AUTO_REZERWACJA
				,@NrZamKlienta=NR_ZAMOWIENIA_KLIENTA
				,@Typ=TYP
				,@IdPracownika=ID_PRACOWNIKA
				,@PrzelicznikWal=PRZELICZNIK_WAL
				,@DataKursWal=DATA_KURS_WAL
				,@SymWal=SYM_WAL
				,@DokWal=DOK_WAL
				,@FlagaStanu=2
				,@TrybRejestracji=0
				,@RabatNarzut=RABAT_NARZUT
				,@ZnakRabatu=case when RABAT_NARZUT>=0 then 2 else 0 end
				,@Uwagi=UWAGI
				,@IdRachunku=ID_RACHUNKU
				,@InformacjeDodatkowe=INFORMACJE_DODATKOWE
				,@OsobaZamawiajaca=OSOBA_ZAMAWIAJACA
				,@IdKontaktu=ID_KONTAKTU
				,@FormaPlatnosci=FORMA_PLATNOSCI
				,@DniPlatnosci=DNI_PLATNOSCI
				,@FakturaParagon=FAKTURA_DO_ZAMOWIENIA
				,@NumerPrzesylki=NUMER_PRZESYLKI
				,@IdOperatoraPrzesylki=ID_OPERATORA_PRZESYLKI
		from	dbo.ZAMOWIENIE 
		where	ID_ZAMOWIENIA=@IdZamowienia

		select	@IdTypu=ID_TYPU
		from	dbo.TYP_DOKUMENTU_MAGAZYNOWEGO
		where	ID_FIRMY=@IdFirmy

		update	dbo.POZYCJA_ZAMOWIENIA
		set		ID_DOSTAWY_REZ=case when @DoRezerwacji<>0 then @IdPozDostawy else 0 end
		where	ID_POZYCJI_ZAMOWIENIA=@IdPozZamowienia

		exec	dbo.RM_PoprawPozycjeZamowienia	@IdPozZamowienia
												,@IdZamowienia
												,@IdArtykulu
												,@KodVAT
												,@Zamowiono
												,@Zrealizowano
												,@Zarezerwowano
												,@DoRezerwacji
												,@CenaNetto
												,@CenaBrutto
												,@CenaNettoWal
												,@CenaBruttoWal
												,@Przelicznik
												,@Jednostka
												,@Narzut
												,@Opis
												,@ZnakNarzutu
												,@IdWariantu
												,@ZnacznikCeny
												,@NrSerii

		exec	dbo.RM_SumujZamowienie @IdZamowienia

		exec	dbo.JL_PobierzFormatNumeracji_Server	@IdFirmy
														,@Dokument
														,@IdTypu
														,@IdMagazynu
														,@NumeracjaFormat output
														,@NumeracjaOkres output
														,@NumeracjaAuto output
														,@NumeracjaNiezalezny output

		exec	dbo.RM_ZatwierdzZamowienie	@IdZamowienia
											,@IdKontrahenta
											,@IdTypu
											,@Numer
											,@NumeracjaFormat
											,@NumeracjaOkres
											,@NumeracjaAuto
											,@NumeracjaNiezalezny
											,@Autonumer
											,@IdFirmy
											,@IdMagazynu
											,@Data
											,@DataRealizacji
											,@Zaliczka
											,@Priorytet
											,@AutoRezerwacja
											,@NrZamKlienta
											,@Typ
											,@IdPracownika
											,@PrzelicznikWal
											,@DataKursWal
											,@SymWal
											,@DokWal
											,@FlagaStanu
											,@TrybRejestracji
											,@RabatNarzut
											,@ZnakRabatu
											,@UWagi
											,@IdRachunku
											,@InformacjeDodatkowe
											,@OsobaZamawiajaca
											,@IdKontaktu
											,@FormaPlatnosci
											,@DniPlatnosci
											,@FakturaParagon
											,@NumerPrzesylki
											,@IdOperatoraPrzesylki
		
		--zakończ transakcję
		commit	transaction kl_ZarezerwujPozycje
		
		return	1
	end try
	begin catch
		declare	@ErrorNumber int=error_number()
				,@ErrorSeverity int=error_severity()
				,@ErrorState int=error_state()
				,@ErrorProcedure varchar(128)=error_procedure()
				,@ErrorLine int=error_line()
				,@ErrorMessage nvarchar(2048)=error_message()

		if @@trancount>0
			rollback	transaction kl_ZarezerwujPozycje

		insert	into [imag].[Log]
				([TypLog]
				,[RodzajLog]
				,[IdFirmy]
				,[IdMagazynu]
				,[IdUzytkownika]
				,[IdObiektu]
				,[Message]
				,[ErrorNumber]
				,[ErrorSeverity]
				,[ErrorState]
				,[ErrorProcedure]
				,[ErrorLine]
				,[ErrorMessage]
				)
		values	('S'--@TypLog
				,0--bład
				,@IdFirmy
				,@IdMagazynu
				,@IdUzytkownika
				,@IdZamowienia
				,@Message
				,@ErrorNumber
				,@ErrorSeverity
				,@ErrorState
				,@ErrorProcedure
				,@ErrorLine
				,@ErrorMessage
				)
		--raiserror (@ErrorMessage, 16, 1)
		--dla wersji SQL>=2016
		;throw
		return 0
	end catch
end



/*
EXEC RM_PoprawPozycjeZamowienia 255141,111714,10527,'23',960,0,0,960,1488,1830.24,0,0,960,'box',20,'VIOLET ',2,0,'k',''  

EXEC RM_SumujZamowienie 111714

EXEC RM_ZatwierdzZamowienie 111714,954,12,'ZO 2355/21','ZO ####/$$',1,1,2,0,1,1,80545,80545,0,2,0,'ZO 4',1,0,1,0,'   ',0,2,0,20,2,'',4,'','',0,'gotówka',0,1,'',0

EXEC RM_ZatwierdzStatusZamowienia 111714,3000001,'N'
*/
GO
