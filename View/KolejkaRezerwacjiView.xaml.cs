﻿using Rezerwacje.ViewModel;
using System.Windows.Controls;

namespace Rezerwacje.View
{
    /// <summary>
    /// Logika interakcji dla klasy KolejkaRezerwacjiView.xaml
    /// </summary>
    public partial class KolejkaRezerwacjiView : UserControl
    {
        public KolejkaRezerwacjiView(decimal idPozDokMag)
        {
            InitializeComponent();
            DataContext = new KolejkaRezerwacjiViewModel(idPozDokMag);
        }
    }
}
