﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rezerwacje.Model
{
    public class PozycjaDokumentuModel :ViewModelExtension
    {
        private decimal _ilosc { get; set; }
        private decimal _iloscSztuk { get; set; }
        private decimal _zarezerwowano { get; set; }
        private decimal _zarezerwowanoSztuk { get; set; }
        private decimal _pozostaloDoRezerwacji { get; set; }
        private decimal _pozostaloDoRezerwacjiSztuk { get; set; }
        public decimal IdPozDokMag { get; set; }
        public int Lp { get; set; }
        public string Nazwa { get; set; }
        public string IndeksHandlowy { get; set; }
        public string NazwaKoloru { get; set; }
        public string NumerKoloru { get; set; }
        public decimal Przelicznik { get; set; }
        public decimal Ilosc
        {
            get => Przelicznik == 1 ? _ilosc : _ilosc / Przelicznik;
            //set => _ilosc = value;
            set { _ilosc = value; OnPropertyChanged("Ilosc"); }
        }
        public string Jednostka { get; set; }
        public decimal IloscSztuk { get => _iloscSztuk; set { _iloscSztuk = value; OnPropertyChanged("IloscSztuk"); } }
        public decimal CenaNetto { get; set; }
        public decimal CenaBrutto { get; set; }
        public decimal Zarezerwowano
        {
            get => Przelicznik == 1 ? _zarezerwowano : _zarezerwowano / Przelicznik;
            //set => _zarezerwowano = value;
            set { _zarezerwowano = value; OnPropertyChanged("Zarezerwowano"); }
        }
        public decimal ZarezerwowanoSztuk { get => _zarezerwowanoSztuk; set { _zarezerwowanoSztuk = value; OnPropertyChanged("ZarezerwowanoSztuk"); } }
        public decimal PozostaloDoRezerwacji
        {
            get => Przelicznik == 1 ? _pozostaloDoRezerwacji : _pozostaloDoRezerwacji / Przelicznik;
            //set => _pozostaloDoRezerwacji = value;
            set { _pozostaloDoRezerwacji = value; OnPropertyChanged("PozostaloDoRezerwacji"); }
        }
        public decimal PozostaloDoRezerwacjiSztuk { get => _pozostaloDoRezerwacjiSztuk; set { _pozostaloDoRezerwacjiSztuk = value; OnPropertyChanged("PozostaloDoRezerwacjiSztuk"); } }
    }
}
