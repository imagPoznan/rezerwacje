﻿using Rezerwacje.ViewModel.Extension;

namespace Rezerwacje
{
    public class WaproModel : ViewModelExtension
    {
        public static WaproModel Instance { get; set; } = new WaproModel();
        private string _sqlServer;
        private string _sqlDatabase;
        private string _sqlUser;
        private string _sqlPassword;
        private decimal _idFirmy;
        private decimal _idMagazynu;
        private decimal _idObiektu;
        private decimal _idUzytkownika;
        private int _zakresOd;
        private int _zakresDo;
        private string _opisZakresu;
        private int _kodKontekstu;
        private decimal _idObiektuNadrzednego;
        public string SqlServer
        {
            get { return _sqlServer; }
            set { _sqlServer = value; OnPropertyChanged("SqlServer"); }
        }
        public string SqlDatabase
        {
            get { return _sqlDatabase; }
            set { _sqlDatabase = value; OnPropertyChanged("SqlDatabase"); }
        }
        public string SqlUser
        {
            get { return _sqlUser; }
            set { _sqlUser = value; OnPropertyChanged("SqlUser"); }
        }
        public string SqlPassword
        {
            get { return _sqlPassword; }
            set { _sqlPassword = value; OnPropertyChanged("SqlPassword"); }
        }
        public decimal IdFirmy
        {
            get { return _idFirmy; }
            set { _idFirmy = value; OnPropertyChanged("IdFirmy"); }
        }
        public decimal IdMagazynu
        {
            get { return _idMagazynu; }
            set { _idMagazynu = value; OnPropertyChanged("IdMagazynu"); }
        }
        public decimal IdObiektu
        {
            get { return _idObiektu; }
            set { _idObiektu = value; OnPropertyChanged("IdObiektu"); }
        }
        public decimal IdUzytkownika
        {
            get { return _idUzytkownika; }
            set { _idUzytkownika = value; OnPropertyChanged("IdUzytkownika"); }
        }
        public int ZakresOd
        {
            get { return _zakresOd; }
            set { _zakresOd = value; OnPropertyChanged("ZakresOd"); }
        }
        public int ZakresDo
        {
            get { return _zakresDo; }
            set { _zakresDo = value; OnPropertyChanged("ZakresDo"); }
        }
        public string OpisZakresu
        {
            get { return _opisZakresu; }
            set { _opisZakresu = value; OnPropertyChanged("OpisZakresu"); }
        }
        public int KodKontekstu
        {
            get { return _kodKontekstu; }
            set { _kodKontekstu = value; OnPropertyChanged("KodKontekstu"); }
        }
        public decimal IdObiektuNadrzednego
        {
            get { return _idObiektuNadrzednego; }
            set { _idObiektuNadrzednego = value; OnPropertyChanged("IdObiektuNadrzednego"); }
        }
    }
}
