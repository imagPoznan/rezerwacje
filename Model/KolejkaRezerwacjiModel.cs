﻿using System;

namespace Rezerwacje.Model
{
    public class KolejkaRezerwacjiModel : ViewModelExtension
    {
        private decimal _zamowiono;
        private decimal _zarezerwowano;
        private decimal _zarezerwowanoSztuk;
        private decimal _doRezerwacji;
        private decimal _doRezerwacjiSztuk;
        private decimal _zrealizowano;
        public int Lp { get; set; }
        public decimal IdZamowienia { get; set; }
        public decimal IdPozZamowienia { get; set; }
        public string Numer { get; set; }
        public string NrZamKlienta { get; set; }
        public string StanRealizacji { get; set; }
        public DateTime Data { get; set; }
        public DateTime DataRealizacji { get; set; }
        public decimal IdKontrahenta { get; set; }
        public string Kontrahent { get; set; }
        public decimal Przelicznik { get; set; }
        public decimal Zamowiono
        {
            get => Przelicznik == 1 ? _zamowiono : _zamowiono / Przelicznik;
            set => _zamowiono = value;
        }
        public string Jednostka { get; set; }
        public decimal ZamowionoSztuk { get; set; }
        public decimal Zarezerwowano
        {
            get => Przelicznik == 1 ? _zarezerwowano : _zarezerwowano / Przelicznik;
            //set => _zarezerwowano = value;
            set
            {
                _zarezerwowano = value;
                //if (_zarezerwowano != 0)
                //{
                //    _doRezerwacji = _zarezerwowano / Przelicznik;
                //}
                //else
                //{
                //    _doRezerwacji = 0;
                //}
                OnPropertyChanged("DoRezerwacji");
            }
        }
        public decimal ZarezerwowanoSztuk
        {
            get => _zarezerwowanoSztuk;
            //set => _zarezerwowanoSztuk = value;
            set
            {
                _zarezerwowanoSztuk = value;
                //if (_zarezerwowanoSztuk != 0)
                //{
                //    _doRezerwacjiSztuk = _zarezerwowanoSztuk;
                //}
                //else
                //{
                //    _doRezerwacjiSztuk = 0;
                //}
                OnPropertyChanged("DoRezerwacjiSztuk");
            }
        }
        public decimal DoRezerwacji
        {
            get => _doRezerwacji;
            //set => _doRezerwacji = Przelicznik == 1 ? value : value * Przelicznik;
            //set => _doRezerwacji = value;
            set
            {
                _doRezerwacji = value;
                _doRezerwacjiSztuk = value * Przelicznik;
                OnPropertyChanged("DoRezerwacji");
                OnPropertyChanged("DoRezerwacjiSztuk");
            }
        }
        public decimal DoRezerwacjiSztuk
        {
            get => _doRezerwacjiSztuk;
            //set => _doRezerwacjiSztuk = value;
            set
            {
                _doRezerwacjiSztuk = value;
                _doRezerwacji = value / Przelicznik;
                OnPropertyChanged("DoRezerwacjiSztuk");
                OnPropertyChanged("DoRezerwacji");
            }
        }
        public decimal Zrealizowano
        {
            get => Przelicznik == 1 ? _zrealizowano : _zrealizowano / Przelicznik;
            set => _zrealizowano = value;
        }
        public decimal ZamowionoSuma { get; set; }
        public decimal ZarezerwowanoSuma { get; set; }
        public decimal ZamowionoSztukSuma { get; set; }
        public decimal ZarezerwowanoSztukSuma { get; set; }
    }
}
