﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Rezerwacje.ViewModel.Extension.LicenseExtension;

namespace Rezerwacje.Model
{
    public class LicenseModel : ViewModelExtension
    {
        private bool _isActive;
        private LicencjaStatus _status;
        private string _dataWygasniecia;
        private string _numerLicencji;
        public bool IsActive
        {
            get => _isActive;
            set
            {
                if (_isActive == value)
                {
                    return;
                }
                _isActive = value;
                OnPropertyChanged("IsActive");
            }
        }
        public LicencjaStatus Status
        {
            get => _status;
            set
            {
                if (_status == value)
                {
                    return;
                }
                _status = value;
                OnPropertyChanged("Status");
            }
        }
        public string DataWygasniecia
        {
            get => _dataWygasniecia;
            set
            {
                _dataWygasniecia = value;
                OnPropertyChanged("DataWygasniecia");
            }
        }
        public string NumerLicencji
        {
            get
            {
                return _numerLicencji;
            }
            set
            {
                _numerLicencji = value;
            }
        }
    }
}
