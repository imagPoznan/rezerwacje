﻿#pragma checksum "..\..\..\View\KolejkaRezerwacjiView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "9D1AF0D2B025859D4546DF646505F8801CFBF2F0113F9CC7981951574F7C6A3F"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using Rezerwacje;
using Rezerwacje.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Rezerwacje.View {
    
    
    /// <summary>
    /// KolejkaRezerwacjiView
    /// </summary>
    public partial class KolejkaRezerwacjiView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.DialogHost KolejkaRezerwacjiDialog;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid MainContent;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Naglowek;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Ilosc;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label IloscSztuk;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Zarezerwowano;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ZarezerwowanoSztuk;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Pozostało;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label PozostałoSztuk;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid Kolejka;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Bottom;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel BottomLeft;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Rezerwuj;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Test;
        
        #line default
        #line hidden
        
        
        #line 198 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel BottomRight;
        
        #line default
        #line hidden
        
        
        #line 204 "..\..\..\View\KolejkaRezerwacjiView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Zamknij;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Rezerwacje;component/view/kolejkarezerwacjiview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\KolejkaRezerwacjiView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.KolejkaRezerwacjiDialog = ((MaterialDesignThemes.Wpf.DialogHost)(target));
            return;
            case 2:
            this.MainContent = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.Naglowek = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.Ilosc = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.IloscSztuk = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.Zarezerwowano = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.ZarezerwowanoSztuk = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.Pozostało = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.PozostałoSztuk = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.Kolejka = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 11:
            this.Bottom = ((System.Windows.Controls.Grid)(target));
            return;
            case 12:
            this.BottomLeft = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 13:
            this.Rezerwuj = ((System.Windows.Controls.Button)(target));
            return;
            case 14:
            this.Test = ((System.Windows.Controls.Button)(target));
            return;
            case 15:
            this.BottomRight = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 16:
            this.Zamknij = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

